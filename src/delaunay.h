/*
 * delaunay.h
 *
 *  Created on: 15 Nov 2018
 *      Author: Mika
 */

#ifndef SRC_DELAUNAY_H_
#define SRC_DELAUNAY_H_


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdint.h>
#include <time.h>
#include "helper.h"


void reportTriangle(vertex, vertex, vertex);
vertex vector(vertex, vertex);
double distance(vertex, vertex);
void printPoint(vertex);
void delaunayCws(vertex*, int);
void write_ppm(const char*);
void clockwiseNextDelaunayEdge(const int *p, const int *q, int *r, const vertex *S, const int *size, int *right);
int firstDelaunayEdgeIncidentTo(int j, vertex* S, int size);
char* vertexToString(vertex);
circle circleFrom3Points(vertex, vertex, vertex);
void circleCenter(const vertex*, const vertex*, const vertex*, vertex*);

#endif /* SRC_DELAUNAY_H_ */
