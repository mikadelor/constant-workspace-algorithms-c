/*
 * voronoi.c
 *
 *  Created on: 23 Nov 2018
 *      Author: Mika
 */

#include "voronoi.h"


int reportEdge(vertex a, vertex b) {
	svgDrawLine(a, b);
	return 0;
}


/**
 * returns the vertex, that you get, when you go right from the center of a and b until you hit the border of the room, where the points are
 * @param a
 * @param b
 * @return
 */
vertex findBorderVertex(vertex a, vertex b) {
	vertex ret = (vertex){-1,-1};
	vertex center = {(a.x+b.x)/2, (a.y+b.y)/2};
	//der Vektor, den man erhaelt, wenn man den Vektor ab nach rechts kippt
	vertex dir = {(b.y-a.y), (a.x-b.x)};
	double m = (0 - center.x)/dir.x;
	double y = center.y + m * dir.y;
	if(m > 0 && (y > 0 && y < 1)) {
		ret = (vertex){0, y};
		return ret;
	}
	m = (1 - center.x)/dir.x;
	y = center.y + m * dir.y;
	if(m > 0 && (y > 0 && y < 1)) {
		ret = (vertex){1, y};
		return ret;
	}
	m = (0 - center.y)/dir.y;
	double x = center.x + m * dir.x;
	if(m > 0 && (x > 0 && x < 1)) {
		ret = (vertex){x, 0};
		return ret;
	}
	m = (1 - center.y)/dir.y;
	x = center.x + m * dir.x;
	if(m > 0 && (x > 0 && x < 1)) {
		ret = (vertex){x, 1};
		return ret;
	}
	return ret;
}

int outOfBorder(vertex v) {
	if(v.y > 1 || v.y < 0 || v.x > 1 || v.x < 0) {
		return 1;
	}
	return 0;
}

void voronoiCws(vertex* S, int size) {
	for(int i = 0; i < size; i++){
		int j = firstDelaunayEdgeIncidentTo(i, S, size);
		int j0 = j;
		vertex lastV;
		vertex currentV;
		vertex firstV;
		int first = 1;
		do {
			int k;
			int isTriangle;
			clockwiseNextDelaunayEdge(&i, &j, &k, S, &size, &isTriangle);
			if(isTriangle == 2) {
				//currentV = vertexCircle2vertex((vertexCircle){S[i], S[j], S[k]});
				circleCenter(&S[i], &S[j], &S[k], &currentV);
				if(first) {
					first = 0;
					firstV = currentV;
				} else {
					if(!outOfBorder(currentV)) {
#ifdef OUTPUTFILE
							reportEdge(currentV, lastV);
#endif
					}
					if(k == j0) {
#ifdef OUTPUTFILE
							reportEdge(currentV, firstV);
#endif
					}
				}
			} else if(isTriangle == 1) {
				//hier ist ein aeusserer Rand der Triangulierung erreicht, also muss der naechste Punkt des Diagramms per Hand ausgerechnet werden
				//lastV = vertexCircle2vertex((vertexCircle){S[i], S[j], S[k]});
				currentV = findBorderVertex(S[j], S[i]);
				if(first) {
					first = 0;
					firstV = currentV;
				} else {
					if(!outOfBorder(lastV)) {
#ifdef OUTPUTFILE
							reportEdge(currentV, lastV);
#endif
					}
				}
				currentV = findBorderVertex(S[i], S[k]);
				if(k == j0) {
					if(!outOfBorder(firstV)) {
						reportEdge(currentV, firstV);
					}
				}
			}
			lastV = currentV;
#ifdef DEL
			if(i < j && i < k) {
				if(isTriangle == 2){
#ifdef OUTPUTFILE
						reportTriangle(S[i], S[j], S[k]);
#endif
				}
			}
#endif
			j = k;
		} while(j != j0);
	}
}


