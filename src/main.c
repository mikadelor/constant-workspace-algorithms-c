
/**
 * use if you want to use command line input
 */
//#define INPUT

/**
 * used for doing the Runtimetest
 */
#define RUNTIMETEST

/*
 * main.c
 *
 *  Created on: 15 Nov 2018
 *      Author: Mika
 */

#include "main.h"

/**
 * 1. function 0=cws_delaunay 1=cws_voronoi 2=dwyer delaunay 3=dwyer voronoi
 * 2. f if a file is used; g if sites should be generated
 * 3. if f: filelocation/filename; if g: number of sites to be generated
 * @param argc should be 5
 * @param argv parameters
 * @return
 */
int main(int argc, char *argv[]) {
	int size;
	int function;
	vertex *points;
#ifdef INPUT
	char *file;
	if(argc != 4) {
		fprintf(stderr, "Input should have 4 Arguments.");
		exit(EXIT_FAILURE);
	}

	function = atoi(argv[1]);
	if(argv[2][0] == 'f'){
		file = argv[3];
		readFile(file, &points, &size);
	} else if(argv[2][0] == 'g') {
		size = atoi(argv[3]);
		points = malloc(size * sizeof(vertex));
		generatePoints(points, size);
#ifdef OUTPUTFILE
		viewboxMinX = 0;
		viewboxMinY = 0;
		viewboxMaxX = 1;
		viewboxMaxY = 1;
#endif
	}
#endif


#ifdef OUTPUTFILE
	out = fopen ( OUTPUTFILE, "w" );
	writeSvgStart ( out );
#endif
#ifdef RUNTIMETEST
	size = 2;
	int line = 2;
	printf(",cwsdel,cwsvon,dwydel,dwyvon,\n");
	for(int k = 0; k < 18; k++) {
		size *= 2;
		printf("%d", size);
		for(int i = 0; i < 10; i++) {
		//	vertex points[size];
			printf(",");
			points = malloc(size * sizeof(vertex));
			generatePoints(points, size);
			for(function = 0; function < 4; function++) {
				clock_t begin = clock();
#endif

				switch(function){
					case 0:
						delaunayCws(points, size);
						break;
					case 1:
						voronoiCws(points, size);
						break;
					case 2:
						dwyer('m', points, size);
						break;
					case 3:
						dwyer('v', points, size);
						break;
					default:
						break;
				}

#ifdef RUNTIMETEST
				clock_t end = clock();
				double number_of_clocks = (double) (end-begin);
				double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;


				printf("%f,", time_spent);
			}
			printf("\n");
			line++;

			free(points);
		}
		printf("average,=AVERAGE(B%d:B%d),=AVERAGE(C%d:C%d),=AVERAGE(D%d:D%d),=AVERAGE(E%d:E%d),\n", line-1, line-10, line-1, line-10, line-1, line-10, line-1, line-10);
		printf("max,=MAX(B%d:B%d),=MAX(C%d:C%d),=MAX(D%d:D%d),=MAX(E%d:E%d),\n", line-1, line-10, line-1, line-10, line-1, line-10, line-1, line-10);
		printf("min,=MIN(B%d:B%d),=MIN(C%d:C%d),=MIN(D%d:D%d),=MIN(E%d:E%d),\n", line-1, line-10, line-1, line-10, line-1, line-10, line-1, line-10);
		line += 3;

	}
#endif









#ifdef OUTPUTFILE
	drawSites(points, size);
	writeSvgEnd ( out );
	fclose(out);
#endif

	return EXIT_SUCCESS;
}

void generatePoints(vertex* points, int size) {
	int seed = time(NULL);
	srand(seed);
	for(int i = 0; i < size; i++) {
		points[i] = (vertex) {(double) rand()/RAND_MAX, (double) rand()/RAND_MAX};
	}
}

void readFile(char *file, vertex **v, int *size){
	FILE *f;
	f=fopen(file,"rt");
	if(f == NULL) {
		fprintf(stderr, "No such file: %s\n", file);
		exit(EXIT_FAILURE);
	}
	if(fscanf(f, "%d\n", size) != 1){
		fprintf(stderr, "Number of sites not specified in %s\n", file);
		exit(EXIT_FAILURE);
	}
	*v = malloc(*size * sizeof(vertex));
	printf("Number of Points: %d\n", *size);
	int i;
#ifdef OUTPUTFILE
	viewboxMinX = (*v)[0].x;
	viewboxMinY = (*v)[0].y;
	viewboxMaxX = (*v)[0].x;
	viewboxMaxY = (*v)[0].y;
#endif
	for(i = 0; i < *size && fscanf(f, "%lf, %lf\n", &(*v)[i].x, &(*v)[i].y) == 2; i++) {
#ifdef OUTPUTFILE
		if(i == 0){
			viewboxMinX = (*v)[0].x;
			viewboxMinY = (*v)[0].y;
			viewboxMaxX = (*v)[0].x;
			viewboxMaxY = (*v)[0].y;
		}
		if((*v)[i].x < viewboxMinX) {
			viewboxMinX = (*v)[i].x;
		} else if ((*v)[i].x > viewboxMaxX){
			viewboxMaxX = (*v)[i].x;
		}
		if((*v)[i].y < viewboxMinY) {
			viewboxMinY = (*v)[i].y;
		} else if((*v)[i].y > viewboxMaxY) {
			viewboxMaxY = (*v)[i].y;
		}
#endif
	}

	if(i != *size) {
		fprintf(stderr, "Number of sites specified in %s does not match with the actual number of points\n", file);
	}
	fclose(f);
}
