/*
 * dwyer.h
 *
 *  Created on: Dec 2, 2018
 *      Author: mika
 */

#ifndef SRC_DWYER_H_
#define SRC_DWYER_H_

#include "helper.h"

dwyer(char, vertex*, int);

#endif /* SRC_DWYER_H_ */
