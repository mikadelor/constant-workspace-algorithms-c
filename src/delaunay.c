/*
 ============================================================================
 Name        : constant-workspace-algorithms-c.c
 Author      : Mika Delor
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include "delaunay.h"




double distanceSquared(vertex p1, vertex p2) {
	double x = p1.x - p2.x;
	double y = p1.y-p2.y;
	return (x*x+y*y);
}



void reportTriangle(vertex a, vertex b, vertex c) {
#ifdef OUTPUTFILE
	svgDrawTriangle(a, b, c);
#endif

}



double det2(double a, double b, double c, double d) {
	return a*d-b*c;
}

void circleCenter(const vertex* v1, const vertex* v2, const vertex* v3,
		vertex* center) {
	double ax = 2 * (v1->x - v2->x);
	double bx = 2 * (v1->y - v2->y);
	double cx = v1->x * v1->x - v2->x * v2->x + v1->y * v1->y - v2->y * v2->y;
	double ay = 2 * (v1->x - v3->x);
	double by = 2 * (v1->y - v3->y);
	double cy = v1->x * v1->x - v3->x * v3->x + v1->y * v1->y - v3->y * v3->y;
	double det = det2(ax, bx, ay, by);
	center->x = det2(cx, bx, cy, by) / det;
	center->y = det2(ax, cx, ay, cy) / det;
}


circle circleFrom3Points(vertex v1, vertex v2, vertex v3) {
	circle c;
	circleCenter(&v1, &v2, &v3, &c.center);
	c.radius = distanceSquared((v1),(c.center));
	return c;
}


/**
 *
 * @param p1
 * @param p2
 * @return the distance between p1 and p2
 */
double distance(vertex p1, vertex p2) {
	return hypot(p1.x - p2.x, p1.y-p2.y);
}



/**
 *
 * @param p
 * @param q
 * @param r
 * @return true, if r lies right of the vector pointing from p to q, false otherwise
 */
int isRight(vertex p, vertex q, vertex r) {
	vertex a;
	vertex b;
	a = vector(p, q);
	b = vector(p, r);
	return a.x*b.y-a.y*b.x > 0;
}

/**
 *
 * @param a Point
 * @param b Point
 * @return the vector pointing from a to b
 */
vertex vector(vertex a, vertex b) {
	double x = a.x - b.x;
	double y = a.y - b.y;
	vertex p = {x, y};
	return p;
}

/**
 * @param q
 * @param p
 * @param r
 * @return angle qpr (angle in p)
 */
double calcAngle(vertex q, vertex p, vertex r) {
	double pq = distance(p, q);
	double pr = distance(p, r);
	double qr = distance(q, r);
	return acos((pq*pq + pr*pr - qr*qr)/(2*pq*pr));
}

int firstDelaunayEdgeIncidentTo(int j, vertex* S, int size) {
	int k;
	double minDist, dist;
	if(j != 0) {
		k = 0;
		minDist = distanceSquared(S[j], S[0]);
	} else /*if(j != 1)*/ {
		k = 1;
		minDist = distanceSquared(S[j], S[1]);
	}
	//TODO erst bei j anfangen? problem: es ist nicht unbedingt die kleinste seite, wenn wir spaeter anfangen
	for(int i = 0; i < size; i++) {
		if(j != i) {
			dist = distanceSquared(S[j], S[i]);
			if(dist < minDist) {
				k = i;
				minDist = dist;
			}
		}
	}
	return k;
}


void clockwiseNextDelaunayEdge(const int *p, const int *q, int *r, const vertex *S, const int *size, int *right) {
	//0: r not initialized, 1: r is left, 2: r is right
	*right = 0;
	double maxAngle;
	circle c;
	//area and angle into 1 var
	double angle;
	for(int i = 0; i < *size; i++) {
		if(i == *p || i == *q) {

		} else if(isRight(S[*p], S[*q], S[i])) {
			//maximize signed area
			//http://mathworld.wolfram.com/TriangleArea.html
			if(*right < 2) {
				//calc circle
				*right = 2;
				*r = i;
				c = circleFrom3Points(S[*p], S[*q], S[*r]);
			} else {
				//if inside circle: calculate new circle, else: do nothing
				if(distanceSquared(c.center, S[i]) < c.radius) {
					*r = i;
					c = circleFrom3Points(S[*p], S[*q], S[*r]);
				}
			}
		} else if(*right < 2) {
			//maximize angle qpr
			angle = calcAngle(S[*q], S[*p], S[i]);
			//TODO is c 'or' lazy, if yes combine if and if else
			//TODO only return edge and not entire triangle, if left
			if(*right < 1) {
				*right = 1;
				maxAngle = angle;
				*r = i;
			} else if(angle > maxAngle){
				maxAngle = angle;
				*r = i;
			}
		}
	}

	//if you want to draw the circle comment this line out
	//svgDrawCircle(c, out);
}



void delaunayCws(vertex* S, int size) {
	//struct Triangle delaunayTriangles[size-2];
	for(int i = 0; i < size; i++){
		//TODO eventuell nur j zurueckgeben (i haben wir ja schon)
		int j = firstDelaunayEdgeIncidentTo(i, S, size);
		// Find the point p[j] e S that is nearest to p[i]
		int j0 = j;
		do {
			//TODO eventuell nur k zurueckgeben (i haben wir ja schon)
			int k;
			int isTriangle;
			clockwiseNextDelaunayEdge(&i, &j, &k, S, &size, &isTriangle);
			if(i < j && i < k) {
				if(isTriangle == 2){
#ifdef OUTPUTFILE
					reportTriangle(S[i], S[j], S[k]);
#endif
				}
			}
			j = k;
		} while(j != j0);
	}
}

