/*
 * voronoi.h
 *
 *  Created on: 23 Nov 2018
 *      Author: Mika
 */

#ifndef SRC_VORONOI_H_
#define SRC_VORONOI_H_

#include "delaunay.h"
#include "helper.h"

/**
 * use, if you want to Draw the dual Delaunay-Triangulation together with the Voronoi-Diagram
 */
//#define DEL


int reportEdge(vertex, vertex);

void voronoiCws(vertex* S, int size);


#endif /* SRC_VORONOI_H_ */
