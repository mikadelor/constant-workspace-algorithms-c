/*
 * draw.c
 *
 *  Created on: 15 Nov 2018
 *      Author: Mika
 */

#include "helper.h"



void svgDrawLine(vertex a, vertex b) {
#ifdef OUTPUTFILE
	fprintf(out, "<line vector-effect=\"non-scaling-stroke\" x1=\"%f\" y1=\"%f\" x2=\"%f\" y2=\"%f\" style=\"stroke:blue;stroke-width:1\" />\n", a.x, a.y, b.x, b.y);
#endif
}

void svgDrawCircle(circle c) {
#ifdef OUTPUTFILE
	fprintf(out, "<circle vector-effect=\"non-scaling-stroke\" cx=\"%f\" cy=\"%f\" r=\"%f\" stroke=\"green\" stroke-width=\"1\" fill=\"none\" />\n", c.center.x, c.center.y, c.radius);
#endif
}

void svgDrawTriangle(vertex a, vertex b, vertex c) {
#ifdef OUTPUTFILE
	fprintf(out, "<polygon vector-effect=\"non-scaling-stroke\" points=\"%f,%f %f,%f %f,%f\" style=\"fill:none;stroke:red;stroke-width:1\" />\n", a.x, a.y, b.x, b.y, c.x, c.y);
#endif
}

void svgDrawSite(vertex v) {
#ifdef OUTPUTFILE
	fprintf(out, "<circle vector-effect=\"non-scaling-stroke\" cx=\"%lf\" cy=\"%lf\" r=\"%lf%%\" stroke=\"black\" stroke-width=\"1\" fill=\"black\" />\n", v.x, v.y, R);
#endif
}

void writeSvgStart () {
#ifdef OUTPUTFILE
    fprintf (out, "<!DOCTYPE html>\n<html>\n<body>\n<svg width=\"%d\" viewBox=\"%f %f %f %f\">\n", W, viewboxMinX, viewboxMinY, viewboxMaxX-viewboxMinX, viewboxMaxY-viewboxMinY);
#endif
}

void writeSvgEnd () {
#ifdef OUTPUTFILE
    fprintf (out, "</svg>\n</body>\n</html>\n");
#endif
}


void drawSites(vertex *v, int n){
#ifdef OUTPUTFILE
	for(int i = 0; i < n; i++) {
		svgDrawSite(v[i]);
	}
	fflush(out);
#endif
}

