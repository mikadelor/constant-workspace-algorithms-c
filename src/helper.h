/*
 * helper.h
 *
 *  Created on: 15 Nov 2018
 *      Author: Mika
 */

#ifndef HELPER_H_
#define HELPER_H_

/**
 * use if you want to have an output
 */
//#define OUTPUTFILE "output.html"

#include <stdio.h>


#ifdef OUTPUTFILE
FILE *out;
double viewboxMinX;
double viewboxMinY;
double viewboxMaxX;
double viewboxMaxY;
#define W 1000 //size of the svg-File
#define R .2 //Point size
#endif


typedef struct VEC2{
	double x, y;
} vertex;

typedef struct {
	vertex center;
	double radius;
} circle;


void writeSvgStart ();
void writeSvgEnd ();
void svgDrawLine(vertex, vertex);
void svgDrawCircle(circle);
void svgDrawTriangle(vertex, vertex, vertex);
void svgDrawSite(vertex);
void drawSites(vertex*, int);

#endif /* HELPER_H_ */
