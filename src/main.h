/*
 * main.h
 *
 *  Created on: Dec 5, 2018
 *      Author: mika
 */

#ifndef MAIN_H_
#define MAIN_H_

#include "delaunay.h"
#include "voronoi.h"
#include "helper.h"

/**
 * randomly generates n sites
 * @param v array the sites are saved to
 * @param size number of generates sites, should be the size of v
 */
void generatePoints(vertex* v, int size);

void readFile(char *file, vertex **v, int *size);


#endif /* MAIN_H_ */
